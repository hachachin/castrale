<?php

require_once 'app/config/config.php';
require_once 'app/class/UTILS.php';
require_once 'app/class/Session.php';
require_once 'app/class/communication.php';


$error = null;
$INFO = null;
if (_MAINTENANCE)
    $INFO = "SYSTEM IS CURRENTLY DOWN FOR MAINTENANCE, TRY AGAIN LATER";
else
    Session::isLoggedIn("User/");


$date = strtotime(date("Y-m-d")." -18 year");
$date = date('Y-m-d', $date);

$errotype = filter_input(INPUT_GET, "errortype", FILTER_DEFAULT);
if (isset($errotype) && $INFO == null)
    switch ($errotype) {
        case "INVALID_EMAIL"    :
            $error = "Invalid Email, please try again";
            break;
        case "INVALID_DATE"    :
            $error = "Invalid Date naissance, this site is only for +18";
            break;
        case "DUPLICATE_COLUMN" :
            $error = "User already exist, maybe you Forgot your password, <a href='#'>click here</a>";
            break;
        case "SYSTEM_ERROR"     :
            $error = "System is down or currently in maintenance, please try again later";
            break;
        case "REQUIRED_ALL"     :
            $error = "Empty inputs, please try again";
            break;
        case "INVALID_USER"     :
            $error = "Invalid user name, must contain more than 8 chars, please try again";
            break;
        default : break;
    }

?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once "includes/head.php"; ?>
</head>

<body class="login-page sidebar-collapse">
<?php if ($INFO != null): ?>
    <div class="container">
        <div class="alert-danger">
            <?= $INFO ?>
        </div>
    </div>
<?php else : ?>
    <!-- Navbar -->
    <?php require_once "includes/nav.php"; ?>
    <!-- end navbar-->

    <div class="modal fade" id="Modal-log" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-top">
                        <div class="alert alert-danger " id='erreur' style="display: block;">
                            <strong>Erreur !</strong> Invalid UserName or Password.
                        </div>
                        <div class="form-top-left">
                            <h3>S'authentifiez-Vous</h3>
                            <p>Entrer votre Nom d'utilisateur et Mot de passe</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <form class="login-form" action="" method="POST">
                        <div class="form-group">
                            <label class="sr-only" for="PSEUDO">Nom d'utilisateur</label>
                            <input type="text" name="PSEUDO" placeholder="Nom d'utilisateur..."
                                   class="form-username form-control" id="login" required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="MDP">Mot de passe</label>
                            <input type="password" name="MDP" id="passwd" placeholder="Mot de passe..."
                                   class="form-password form-control" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button id="log-btn" type="submit" class="btn">Se Connecter <i class="fa fa-sign-in"></i></button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="wrapper">

        <div id="page-header" class="page-header" filter-color="orange">
        </div>
        <div class="container">
            <div class="content-center" style="text-align: center;">
                <div class="card" id="card-log">
                    <div id="main-card-body" class="card-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div id="slide-card" class="card">
                                    <div id="slide-card-body" class="card-body">
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div id="carousel-inner" class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <img class="d-block img-fluid" src="assets/img/login1.jpg"
                                                         alt="First slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block img-fluid" src="assets/img/login1.jpg"
                                                         alt="Second slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block img-fluid" src="assets/img/login1.jpg"
                                                         alt="Third slide">
                                                </div>
                                            </div>
                                            <div class="carousel-caption d-none d-md-block">
                                                <h3>HOLA</h3>
                                                <p>jksgg qduihka igiudg sdixg</p>
                                                <button id="profile-btn" type="button" class="btn btn-primary"
                                                        data-toggle="modal" data-target="#Modal">
                                                    Créer profil
                                                </button>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls"
                                               role="button"
                                               data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls"
                                               role="button"
                                               data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="Modal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalLabel">Créer Profil</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="formulaire" class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <form id="regForm" action="REST/kernel.php" method="POST">
                                            <img src="assets/img/star-icon.png" style="height: 120px">
                                            <h5 class="card-title">Créer votre Profil en répondant à quelques questions
                                                :</h5>

                                            <!-- One "tab" for each step in the form: -->
                                            <div class="tab">
                                                <p class="card-text">Tu es...</p>
                                                <div class="btn-group-vertical">
                                                    <button name="GENDER" value="M" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('gender','M');nextPrev(1);">
                                                        Homme
                                                    </button>
                                                    <br>
                                                    <button name="GENDER" value="F" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('gender','F');nextPrev(1);">
                                                        Femme
                                                    </button>
                                                </div>
                                                <input name="GENDER" type="hidden" value="" id="gender">
                                            </div>
                                            <!--<div class="tab">
                                                <a id="prevBtn" onclick="nextPrev(-1)"><span
                                                            class="fa fa-chevron-circle-left fa-2x">
												</span></a>
                                                <p class="card-text">Tu cherches des...</p>
                                                <div class="btn-group-vertical">
                                                    <button name="ORIENTATION" value="M" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('or','M');nextPrev(1);">
                                                        Hommes
                                                    </button>
                                                    <br>
                                                    <button name="ORIENTATION" value="F" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('or','F');nextPrev(1);">
                                                        Femmes
                                                    </button>
                                                </div>
                                                <input name="ORIENTATION" type="hidden" value="" id="or">
                                            </div>-->

                                            <div class="tab">
                                                <div class="alert alert-danger " id='erreur1' style="display:none;">
                                                </div>

                                                <a id="prevBtn" onclick="nextPrev(-1)"><span
                                                            class="fa fa-chevron-circle-left fa-2x">
												</span></a>

                                                <p class="card-text">Anniversaire</p>
                                                <div class="input-group date">
                                                    <input type="date" max="<?=$date?>" name="DATE_NAISSANCE"   required title="10 characters, date format Year-month-day" type="text"
                                                           placeholder="Date de naissance YYYY-MM-DD"
                                                           class="form-control pull-right" id="date">
                                                </div>
                                                <input type="hidden" value="none" name="LOCALIZATION" id="LOCALIZATION">
                                                <select name="PAYS" class="form-control" id="country"
                                                        name="country"></select>
                                                <select name="VILLE" class="form-control" name="state"
                                                        id="state"></select>
                                                <button type="button" id="nextBtn"
                                                        class="form-control btn btn-secondary"
                                                        onclick="nextPrev(1);">Next
                                                </button>
                                            </div>
                                            <div class="tab">
                                                <div class="alert alert-danger " id='erreur2' style="display: none;">
                                                    <strong>Erreur !</strong> Veulliez saisire un nom sans symbole.
                                                </div>
                                                <a id="prevBtn" onclick="nextPrev(-1)"><span
                                                            class="fa fa-chevron-circle-left fa-2x">
												</span></a>
                                                <p class="card-text">Continuer</p>
                                                <p><input id="nom" onkeyup="checkLength(this,4,'erreur2')"    name="NOM"  type="text" class="form-control"
                                                          placeholder="Nom"></p>
                                                <p><input id="prenom" onkeyup="checkLength(this,4,'erreur2')" name="PRENOM" type="text"
                                                          class="form-control"
                                                          placeholder="Prenom"></p>
                                                <button type="button" id="nextBtn"
                                                        class="form-control btn btn-secondary"
                                                        onclick="nextPrev(1);">Next
                                                </button>
                                            </div>

                                            <div class="tab">
                                                <div class="alert alert-danger " id='erreur3' style="display: none;">
                                                    <strong>Erreur !</strong> Cette adresse email est deja utilisé.
                                                </div>
                                                <a id="prevBtn" onclick="nextPrev(-1)"><span
                                                            class="fa fa-chevron-circle-left fa-2x">
												</span></a>
                                                <p class="card-text">Finir</p>
                                                <p><input name="PSEUDO" class="form-control" onkeyup="checkLength(this,8,'erreur3')" type="text"
                                                          placeholder="Pseudo"></p>
                                                <p><input name="EMAIL" class="form-control" type="email"
                                                          placeholder="Email">
                                                </p>
                                                <p><input name="MDP" class="form-control" onkeyup="checkLength(this,8,'erreur3')""
                                                          type="password"
                                                          placeholder="Mot de passe"></p>
                                                <p>Le mot de passe doit comprendre au moins 8 caractères Min et Maj et
                                                    des
                                                    chiffres</p>
                                                <button type="submit" name="ACTION" id="valider" value="addUser"
                                                        class="form-control btn btn-secondary">
                                                    Créer profil
                                                </button>
                                                <p>En continuant, tu confirmes avoir lu et accepté nos <a href="#">Conditions
                                                        Générales d'Utilisation</a>, notre <a href="">Politique de
                                                        Confidentialité </a>ainsi que notre <a href="">Politique en
                                                        matière
                                                        de Cookies</p>
                                            </div>
                                            <div style="text-align:center;margin-top:40px;">
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
<?php endif; ?>
<?php require_once "includes/footer.php"; ?>

</body>

</html>