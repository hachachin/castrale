<?php
/**
 * Created by PhpStorm.
 * User: chadi
 * Date: 04/05/18
 * Time: 20:05
 */
require_once "../app/config/config.php";
require_once "../app/class/Session.php";

if(!Session::isLoggedIn())
{
    //echo "not logged in";
    header("location:../index.php");
    die();
}
global  $Session;
$Session=Session::getInstance();
$Session->start(_SESSION,_SECURE,_HTTPONLY);
//var_dump($Session->get("LAST_ACTIVITY"));
?>
<html>
<head>
<title>User space</title>
</head>
<body>
<h1>Work in progress</h1>
<form action="../REST/logout.php" method="POST">
    <input name="token" type="hidden" value="<?=$Session->get("CSRF")?>">
    <button type="submit">Logout</button>
</form>
</body>
</html>
