<?php

require_once 'app/config/config.php';
require_once 'app/class/UTILS.php';
require_once 'app/class/Session.php';
require_once 'app/class/communication.php';


$error = null;
$INFO = null;
if (_MAINTENANCE)
    $INFO = "SYSTEM IS CURRENTLY DOWN FOR MAINTENANCE, TRY AGAIN LATER";
else {
        Session::isLoggedIn("User/");
    }

$date = strtotime(date("Y-m-d") . " -18 year");
$date = date('Y-m-d', $date);

$errotype = filter_input(INPUT_GET, "errortype", FILTER_DEFAULT);
if (isset($errotype) && $INFO == null)
    switch ($errotype) {
        case "INVALID_EMAIL"    :
            $error = "Invalid Email, please try again";
            break;
        case "INVALID_DATE"    :
            $error = "Invalid Date naissance, this site is only for +18";
            break;
        case "DUPLICATE_COLUMN" :
            $error = "User already exist, maybe you Forgot your password, <a href='#'>click here</a>";
            break;
        case "SYSTEM_ERROR"     :
            $error = "System is down or currently in maintenance, please try again later";
            break;
        case "REQUIRED_ALL"     :
            $error = "Empty inputs, please try again";
            break;
        case "INVALID_USER"     :
            $error = "Invalid user name, must contain more than 8 chars, please try again";
            break;
        case "INVALID_LOCATION":
            $error = "Invalid pays or ville, please try again";
            break;
        default :
            $error = "Something went wrong, please try again";
            break;
    }

?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once "includes/head.php"; ?>
</head>

<body class="login-page sidebar-collapse">
<?php if ($INFO != null): ?>
    <div class="container">
        <div class="alert-danger">
            <?= $INFO ?>
        </div>
    </div>
<?php else : ?>
    <!-- Navbar -->
    <?php require_once "includes/nav.php"; ?>
    <!-- end navbar-->
    <?php if ($error != null) : ?>
        <div class="modal fade" id="Modal-log1" tabindex="-1"  role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="login-form" >
                        <div class="modal-body row">
                            <div class="col-lg-2">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                <span class="fa fa-close"></span>
                            </button>
                            </div>
                            <div class="col-lg-10">
                            <?= $error ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="modal fade" id="Modal-log" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="login-form" >
                <div class="modal-body">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>S'authentifiez-Vous</h3>
                            <p>Entrer votre Nom d'utilisateur et Mot de passe</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <div class="alert alert-danger " id='login' style="display: none;">

                    </div>
                        <div class="form-group">
                            <label class="sr-only" for="PSEUDO">Nom d'utilisateur</label>
                            <input type="text" name="PSEUDO" placeholder="Nom d'utilisateur..."
                                   class="form-username form-control" id="psseudo" required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="MDP">Mot de passe</label>
                            <input type="password" name="MDP" id="passsword" placeholder="Mot de passe..."
                                   class="form-password form-control" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button id="log-btn" onclick="login()" class="btn">Se Connecter <i class="fa fa-sign-in"></i></button>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">

        <div id="page-header" class="page-header" filter-color="orange">
        </div>
        <div class="container">
            <div class="content-center" style="text-align: center;">
                <div class="card" id="card-log">
                    <div id="main-card-body" class="card-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <div id="slide-card" class=" card">
                                        <div id="slide-card-body" class="card-body">
                                            <div id="carouselExampleControls" class="carousel slide"
                                                 data-ride="carousel">
                                                <div id="carousel-inner" class="carousel-inner" role="listbox">
                                                    <div class="carousel-item active">
                                                        <img class="d-block img-fluid" height='342 px' src="<?=$_paths['assets']?>img/galaxy.jpg"
                                                             alt="First slide">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img class="d-block img-fluid" height='342 px'  src="<?=$_paths['assets']?>img/galaxy.jpg"
                                                             alt="Second slide">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img class="d-block img-fluid" height='342 px' src="<?=$_paths['assets']?>img/galaxy.jpg"
                                                             alt="Third slide">
                                                    </div>
                                                </div>
                                                <div class="carousel-caption d-none d-md-block">
                                                    <h3>HOLA</h3>
                                                    <p>jksgg qduihka igiudg sdixg</p>
                                                    <button id="profile-btn" type="button" class="btn btn-primary"
                                                            data-toggle="modal" data-target="#Modal">
                                                        Créer profil
                                                    </button>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleControls"
                                                   role="button"
                                                   data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleControls"
                                                   role="button"
                                                   data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="#formulaire">
                                        <div class="card signup">
                                            s'inscrire maintenant
                                        </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card card-body"  style="">
                                        <p>
                                            Il est dit qu'il y a les âmes par couples. Quand un être est créé, sa
                                            partenaire féminine l’est en même temps que lui.

                                            À cet instant surgit cette question qui nous perturbe tous un jour : qui
                                            est-il, quelle est-elle, cet être avec qui réaliser l’union parfaite,
                                            celui que nous cherchons sans cesse.

                                            Cette rencontre peut être perturbante, déstabilisante et bouleversante
                                            parce que c’est une relation différente de toutes celles que vous avez
                                            connues auparavant, aucun mot ou aucune explication peuvent clairement
                                            exprimer une telle connexion, quand vous la trouverez, votre âme sœur
                                            vous donnera un sentiment d’intégralité, c’est un partage d’énergie, un
                                            savoir intuitif qui semble parfait.

                                            L’espace ou le temps n’existe plus : vous vous êtes trouvés
                                            mutuellement, même si cet être ne correspond pas à l’idéal que vous vous
                                            étiez représenté.
                                        </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="Modal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="ModalLabel">Créer Profil</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="formulaire" class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <form id="regForm" action="REST/kernel.php" method="POST" onsubmit="return validateForm(); ">
                                            <img src="<?=$_paths['assets']?>img/star-icon.png" style="height: 120px">
                                            <h5 class="card-title">Créer votre Profil en répondant à quelques questions
                                                :</h5>

                                            <!-- One "tab" for each step in the form: -->
                                            <div class="tab">
                                                <p class="card-text">Vous ètes...</p>
                                                <div class="btn-group-vertical">
                                                    <button name="GENDER" value="M" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('gender','M');nextPrev(1);">
                                                        Homme
                                                    </button>
                                                    <br>
                                                    <button name="GENDER" value="F" type="button" id="nextBtn"
                                                            class="form-control btn btn-secondary"
                                                            onclick="setGender('gender','F');nextPrev(1);">
                                                        Femme
                                                    </button>
                                                </div>
                                                <input name="GENDER" type="hidden" value="" id="gender">
                                            </div>

                                            <div class="tab">
                                                <button type="button" id="prevBtn" onclick="nextPrev(-1)"
                                                        class="btn btn-primary btn-icon btn-round">
                                                    <i class="fa fa-chevron-left "></i></button>

                                                <p class="card-text">Anniversaire</p>
                                                <div class="alert alert-danger " id='errorDate' style="">
                                                    <span id="checkdate" class="fa fa-close "></span> valid date, and +18<br>
                                                </div>
                                                <div class="input-group date">
                                                    <input type="date" max="<?= $date ?>" name="DATE_NAISSANCE"
                                                           onkeyup="checktab1();"  onchange="checktab1()"
                                                           placeholder="Date de naissance YYYY-MM-DD"
                                                           class="form-control pull-right" id="date">
                                                </div>
                                                <input type="hidden" value="none" name="LOCALIZATION" id="LOCALIZATION">
                                                <select name="PAYS" class="form-control" id="country">

                                                </select>
                                                <select name="VILLE" class="form-control"
                                                        id="state"></select>
                                                <button type="button" id="nextBtn"
                                                        class="form-control btn btn-secondary"
                                                        onclick="nextPrev(1);">Next
                                                </button>
                                            </div>

                                            <div class="tab">
                                                <div class="alert alert-danger " id='errorNom' style="">
                                                    <span id="checknom" class="fa fa-close "></span> 4 chars or more<br>
                                                </div>
                                                <div class="alert alert-danger " id='errorPrenom' style="">
                                                    <span id="checkprenom" class="fa fa-close "></span> 4 chars or more
                                                </div>
                                                <button type="button" id="prevBtn" onclick="nextPrev(-1)"
                                                        class="btn btn-primary btn-icon btn-round">
                                                    <i class="fa fa-chevron-left "></i></button>
                                                <p class="card-text">Continuer</p>
                                                <p><input id="nom" onkeyup="checkNames()" name="NOM" type="text"
                                                          class="form-control"
                                                          placeholder="Nom"></p>
                                                <p><input id="prenom" onkeyup="checkNames()" name="PRENOM" type="text"
                                                          class="form-control"
                                                          placeholder="Prenom"></p>
                                                <button type="button" id="nextBtn"
                                                        class="form-control btn btn-secondary"
                                                        onclick="nextPrev(1);">Next
                                                </button>
                                            </div>

                                            <div class="tab">
                                                <div class="alert alert-danger " id='errorPseudo' style="">
                                                    <span id="checkpseudo" class="fa fa-close "></span>
                                                    Pseudo more than 6 chars, no special chars
                                                </div>
                                                <div class="alert alert-danger " id='errorEmail' style="">
                                                    <span id="checkemail" class="fa fa-close "></span>
                                                    Valid email
                                                </div>

                                                <button type="button" id="prevBtn" onclick="nextPrev(-1)"
                                                        class="btn btn-primary btn-icon btn-round">
                                                    <i class="fa fa-chevron-left "></i></button>

                                                <p class="card-text">Finir</p>
                                                <p><input name="PSEUDO" id="pseudo" class="form-control"
                                                          onkeyup="checkLasStep()" type="text"
                                                          placeholder="Pseudo"></p>
                                                <p><input name="EMAIL" id="email" class="form-control" type="text"
                                                          onkeyup="checkLasStep()" placeholder="Email">
                                                </p>
                                                <p><input name="MDP" class="form-control" onkeyup="checkLasStep()"
                                                          type="password" id="password"
                                                          placeholder="Mot de passe">
                                                </p>

                                                <div class="alert alert-danger " id='errorPassword' style="">
                                                    <span id="checkpassword" class="fa fa-close "></span>
                                                    Le mot de passe doit comprendre au moins 6
                                                    caractères
                                                </div>

                                                <button type="submit" name="ACTION" id="valider" value="addUser"
                                                        class="form-control btn btn-secondary">
                                                    Créer profil
                                                </button>
                                                <p>En continuant, tu confirmes avoir lu et accepté nos <a href="#">Conditions
                                                        Générales d'Utilisation</a>, notre <a href="">Politique de
                                                        Confidentialité </a>ainsi que notre <a href="">Politique en
                                                        matière
                                                        de Cookies</a></p>
                                            </div>
                                            <div style="text-align:center;margin-top:40px;">
                                                <span class="step active finish"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
<?php endif; ?>
<?php require_once "includes/footer.php"; ?>

</body>

</html>