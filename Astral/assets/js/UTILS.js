$(document).ready(function () {
    $navbar = $('.navbar[color-on-scroll]');
    scroll_distance = $navbar.attr('color-on-scroll') || 100;
    if ($('.navbar[color-on-scroll]').length != 0) {


        nowuiKit.checkScrollForTransparentNavbar();
        $(window).on('scroll', nowuiKit.checkScrollForTransparentNavbar)
    }
    checkScrollForTransparentNavbar: debounce(function () {
        if ($(document).scrollTop() > scroll_distance) {
            if (transparent) {

                transparent = false;
                $('.navbar[color-on-scroll]').removeClass('navbar-transparent');
            }
        } else {
            if (!transparent) {
                transparent = true;
                $('.navbar[color-on-scroll]').addClass('navbar-transparent');
            }
        }
    }, 17)
});

function setGender(racin, gen) {
    document.getElementById(racin).value = gen;
}

function geocodeAddress(address) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            setGender('LOCALIZATION', results[0].geometry.location);
            alert(results[0].geometry.location);
        } else {
        }
    });
}

function getselectedvalue() {
    var e = document.getElementById("country");
    var country = e.options[e.selectedIndex].value;
    var ee = document.getElementById("state");
    var city = ee.options[ee.selectedIndex].value;
    geocodeAddress(city + " " + country);
}

function checkLength(el, long, id) {
    if (el.value.length < long) {
        $("#" + id).fadeIn("slow");
        document.getElementById(id).innerHTML = "<b>" + el.getAttribute("name")
            + "</b> length must be more than " + long + " characters";
        el.classList.add("invalid");
        return false;
    } else {
        $("#" + id).fadeOut('fast');
        el.classList.remove("invalid");
        return true;
    }
}

function checkNames() {
    var Checknom = document.getElementById("checknom");
    var Checkprenom = document.getElementById("checkprenom");
    var errorprenom = document.getElementById("errorPrenom");
    var errornom = document.getElementById("errorNom");
    var nom = document.getElementById("nom");
    var prenom = document.getElementById("prenom");

    var n=nom.value.replace(/[0-9]/g,'');
    var p=prenom.value.replace(/[0-9]/g, '');

    nom.value=n;
    prenom.value=p;

    if (nom.value.length < 4) {
        Checknom.className = "fa fa-close";
        nom.classList.add("invalid");
        errornom.classList.remove("alert-success");
        errornom.classList.add("alert-danger");
    }
    else {
        errornom.classList.remove("alert-danger");
        errornom.classList.add("alert-success");
        Checknom.className = "fa fa-check";
        nom.classList.remove("invalid");
    }

    if (prenom.value.length < 4) {
        errorprenom.classList.remove("alert-success");
        errorprenom.classList.add("alert-danger");
        Checkprenom.className = "fa fa-close";
        prenom.classList.add("invalid");
    } else {
        errorprenom.classList.remove("alert-danger");
        errorprenom.classList.add("alert-success");
        Checkprenom.className = "fa fa-check";
        prenom.classList.remove("invalid");
    }

}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkLasStep() {
    var Checkpseudo = document.getElementById("checkpseudo");
    var Checkemail = document.getElementById("checkemail");
    var Checkpassword = document.getElementById("checkpassword");
    var errorpseudo = document.getElementById("errorPseudo");
    var erroremail = document.getElementById("errorEmail");
    var errorpassword = document.getElementById("errorPassword");
    var pseudo = document.getElementById("pseudo");
    var email = document.getElementById("email");
    var password = document.getElementById("password");

    //alert("pseudo : "+pseudo.value+" email : "+email.value+" pass: "+password.value);
    var p=pseudo.value.replace(/[^\w\s]/gi, '');
    pseudo.value=p;

    if (pseudo.value.length < 6) {
        Checkpseudo.className = "fa fa-close";
        pseudo.classList.add("invalid");
        errorpseudo.classList.remove("alert-success");
        errorpseudo.classList.add("alert-danger");
    }
    else {
        errorpseudo.classList.remove("alert-danger");
        errorpseudo.classList.add("alert-success");
        Checkpseudo.className = "fa fa-check";
        pseudo.classList.remove("invalid");
    }

    if (!validateEmail(email.value)) {
        erroremail.classList.remove("alert-success");
        erroremail.classList.add("alert-danger");
        Checkemail.className = "fa fa-close";
        email.classList.add("invalid");
    } else {
        erroremail.classList.remove("alert-danger");
        erroremail.classList.add("alert-success");
        Checkemail.className = "fa fa-check";
        email.classList.remove("invalid");
    }

    if (password.value.length < 6) {
        Checkpassword.className = "fa fa-close";
        password.classList.add("invalid");
        errorpassword.classList.remove("alert-success");
        errorpassword.classList.add("alert-danger");
    }
    else {
        errorpassword.classList.remove("alert-danger");
        errorpassword.classList.add("alert-success");
        Checkpassword.className = "fa fa-check";
        password.classList.remove("invalid");
    }

}

function isValidDate(dateString) {

    //language=JSRegexp
    var regEx;
    regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (dateString.match(regEx) === null || !dateString.match(regEx) || dateString === "") return false;  // Invalid format
    var d = new Date(dateString);
    var today=new Date();
    if (!d.getTime() && d.getTime() !== 0) return false; // Invalid date
    if ((today.getFullYear() - d.getFullYear()) < 18) return false;
    else
        return true;
}

function checktab1() {
    var Checkdate = document.getElementById("checkdate");
    var errordate = document.getElementById("errorDate");
    var date = document.getElementById("date");
    if (isValidDate(date.value)) {
        errordate.classList.remove("alert-danger");
        errordate.classList.add("alert-success");
        Checkdate.className = "fa fa-check";
        date.classList.remove("invalid");
    } else {
        errordate.classList.remove("alert-success");
        errordate.classList.add("alert-danger");
        Checkdate.className = "fa fa-close";
        date.classList.add("invalid");
    }

}

function login() {
    var username=document.getElementById('psseudo').value;
    var password=document.getElementById('passsword').value;
    if(username==="" || password==="")
    {
        document.getElementById('login').innerHTML=" Fill all the fields";
        document.getElementById('login').style.display='block';
        return false;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            var answer= this.responseText;
            //alert(answer);
            switch(answer)
            {
                case "TRUE":
                    window.location.replace("index.php");
                    break;
                case "FALSE":
                    document.getElementById('login').innerHTML="<strong>Erreur !</strong> Invalid UserName or Password.";
                    document.getElementById('login').style.display='block';
                    break;
                case "Exception" :
                    document.getElementById('login').innerHTML="<strong>Fatal Erreur !</strong> System is down, please try again later.";
                    document.getElementById('login').style.display='block';
                    break;
                case "UNKNOWN" :
                    document.getElementById('login').innerHTML="<strong>Fatal Erreur !</strong> Empty request refused by the server!.";
                    document.getElementById('login').style.display='block';
                    break;
                case "SYS_ERROR" :
                    document.getElementById('login').innerHTML="<strong>Cant connect to the server</strong> please try again later.";
                    document.getElementById('login').style.display='block';
                    break;
                default :
                    document.getElementById('login').innerHTML="<strong>System is down</strong> please try again later.<br>"+answer;
                    document.getElementById('login').style.display='block';
                    break;
            }
        }
    };
    xhttp.open("GET", "REST/Login.php?pseudo="+username+"&password="+password, true);
   // alert("user = "+username+" pass="+password+" REST/Login.php?pseudo="+username+"&password="+password);
    xhttp.send();
}

$(window).load(function () {
    $('#Modal-log1').modal('show');
});

/*function checktab3()
 {
 var e = document.getElementById("country");
 var date=document.getElementById('date').value;
 var country=e.options[e.selectedIndex].value;
 e = document.getElementById("state");
 var state=e.options[e.selectedIndex].value;
 var error=document.getElementById('erreur1');
 error.style="";
 error.style="display:none";
 alert(date+country+state);
 if(date!="" )
 {
 error.style="";
 error.style="display:block";
 error.innerHTML="<strong>Erreur !</strong> fill all the inputs";
 return false;
 }else
 {
 getselectedvalue();
 nextPrev(1);
 return true;
 }
 }

 function checktab4()
 {
 var nom=document.getElementById('nom').value;
 var prenom=document.getElementById('prenom').value;
 var error=document.getElementById('erreur2');
 error.style="display:none";

 if(nom!="" || prenom!="" )
 {
 error.style="display:block";
 error.innerHTML="<strong>Erreur !</strong> fill all the inputs";
 return false;
 }
 if(nom.length<4 || prenom.length<4)
 {
 error.style="display:block";
 error.innerHTML="<strong>Erreur !</strong> nom and prenom must be longer than 4chars";
 return false;
 }else
 {
 nextPrev(1);
 }
 }
 */