<?php

$password = filter_input(INPUT_POST,'password',FILTER_SANITIZE_SPECIAL_CHARS);
$hash = null;
$test=filter_input($password);

if (isset($password))
	$hash = password_hash($password, PASSWORD_DEFAULT);

?>
<html>
<head>

</head>
<body>
	<?php if(isset($password)): ?>
	<p>
		<strong>Password :</strong> <?=$password?>
		<br />
		<strong>Hash :</strong> <?=($hash === false ? 'Failed.' : $hash)?>
	</p>
	<?php endif; ?>
	<form method="post">
		<label for ="password">Password : </label>
		<input type="text" name="password" value="<?=$password?>">
		<button type="submit">Hash</button>
	</form>
</body>
</html>
