<?php
require_once "../app/config/config.php";
require_once "../app/class/Entity/UTILISATEUR.php";
require_once "../app/class/Session.php";
require_once "../app/class/UTILS.php";


$email = filter_input(INPUT_GET,'pseudo', FILTER_SANITIZE_SPECIAL_CHARS);
$password = filter_input(INPUT_GET,'password', FILTER_SANITIZE_SPECIAL_CHARS);

if (!isset($password) || !isset($email)) {
    die("UNKNOWN");
}

try {
    $obj = new UTILS();

    $champs = array();
    $champs[] = "pseudo";
    $champs[] = "email";

    $test = array();
    $test[] = "=";
    $test[] = "=";
    $t = array();

    $t[] = $email;
    $t[] = $email;
    $log = array();
    $log[] = "";
    $log[] = "or";

    $row = $obj->selectCondition("UTILISATEUR", $champs, $t, $test, "", $log);
    if ($row== null)
        die("FALSE");

    if(!password_verify($password,$row[0]->getMDP()))
       die("FALSE");

    global $S;
    $S=Session::getInstance();
    $S->start(_SESSION,_SECURE,_HTTPONLY);
    $S->set("USER",$row[0]);
    $S->set("CSRF",$S::newCSRF());
    $S->set("LAST_ACTIVITY",date("Y-m-d H:i:s"));

    die("TRUE");

} catch (PDOException $e) {
    if (_VERBOSE)
        throw $e;
    Session::log($e->getCode()." ".$e->getMessage());
    die("Exception");


}


?>