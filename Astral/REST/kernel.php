<?php
/**
 * Created by PhpStorm.
 * User: chadi
 * Date: 28/04/18
 * Time: 12:35
 */
require_once "../app/config/config.php";
require_once "../app/class/Session.php";
require_once "../app/class/UTILS.php";


$action = filter_input(INPUT_POST, 'ACTION', FILTER_SANITIZE_SPECIAL_CHARS);
var_dump($_POST);

if (isset($action) && !is_null($action)) {
    switch ($action) {
        case "addUser": {

            if (
                isset($_POST["PSEUDO"]) && !is_null($_POST["PSEUDO"])
                && isset($_POST["GENDER"]) && !is_null($_POST["GENDER"])
                && isset($_POST["NOM"]) && !is_null($_POST["NOM"])
                && isset($_POST["PRENOM"]) && !is_null($_POST["PRENOM"])
                && isset($_POST["MDP"]) && !is_null($_POST["MDP"])
                && isset($_POST["EMAIL"]) && !is_null($_POST["EMAIL"])
                && isset($_POST["DATE_NAISSANCE"]) && !is_null($_POST["DATE_NAISSANCE"])
                && isset($_POST["PAYS"]) && !is_null($_POST["PAYS"])
                && isset($_POST["VILLE"]) && !is_null($_POST["VILLE"])
                && isset($_POST["LOCALIZATION"]) && !is_null($_POST["LOCALIZATION"])
            ) {

                $email = strtolower(filter_var($_POST["EMAIL"], FILTER_SANITIZE_EMAIL));
                $password = filter_input(INPUT_POST, 'MDP', FILTER_SANITIZE_SPECIAL_CHARS);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    header("location:../index.php?errortype=INVALID_EMAIL");
                    die();
                }

                $localization = filter_input(INPUT_POST, 'LOCALIZATION', FILTER_SANITIZE_SPECIAL_CHARS);

                $user = array();
                $user[] = filter_input(INPUT_POST, 'PSEUDO', FILTER_SANITIZE_SPECIAL_CHARS);

                if (strlen($user[0]) <= 5) {
                    header("location:../index.php?errortype=INVALID_USER");
                    die();
                }


                $timeDiffrence=date_diff(
                    date_create(filter_input(INPUT_POST, 'DATE_NAISSANCE', FILTER_SANITIZE_SPECIAL_CHARS))
                    ,date_create(date("Y-m-d")));

                if($timeDiffrence->y<18)
                {
                    header("location:../index.php?errortype=INVALID_DATE");
                    die();
                }
                $ville=filter_input(INPUT_POST, 'VILLE', FILTER_SANITIZE_SPECIAL_CHARS);
                $pays=filter_input(INPUT_POST, 'PAYS', FILTER_SANITIZE_SPECIAL_CHARS);

                if($ville=="0" || $pays=="0")
                {
                    header("location:../index.php?errortype=INVALID_LOCATION");
                    die();
                }

                $user[] = filter_input(INPUT_POST, 'GENDER', FILTER_SANITIZE_SPECIAL_CHARS);
                $user[] = ucfirst(strtolower(filter_input(INPUT_POST, 'NOM', FILTER_SANITIZE_SPECIAL_CHARS)));
                $user[] = ucfirst(strtolower(filter_input(INPUT_POST, 'PRENOM', FILTER_SANITIZE_SPECIAL_CHARS)));
                $user[] = password_hash($password, PASSWORD_DEFAULT);
                $user[] = $email;
                $user[] = filter_input(INPUT_POST, 'DATE_NAISSANCE', FILTER_SANITIZE_SPECIAL_CHARS);
                $user[] = $pays . ", " .$ville;
                $user[] = ucfirst(strtolower(filter_input(INPUT_POST, 'DESCRIPTION', FILTER_SANITIZE_SPECIAL_CHARS)));
                $user[] = str_replace("(", "", str_replace(")", "", $localization));
                $user[] = date("Y-m-d H:i:s");
                $user[] = 0;

                try {
                    $obj = new UTILS();
                    $obj->add("UTILISATEUR", $user);

                    $S=Session::getInstance();
                    $S->start(_SESSION,_SECURE,_HTTPONLY);
                    $S->set("CSRF",$S::newCSRF());
                    $S->set("USER",new UTILISATEUR($user));
                    $S->set("LAST_ACTIVITY",date("Y-m-d H:i:s"));
                    header("location:../User/index.php");
                    var_dump($user);
                } catch (Exception $e) {

                    if (_VERBOSE)
                        throw $e;

                    if ($e->getCode() == 1062 || $e->getCode() == 23000) {
                        header("location:../index.php?errortype=DUPLICATE_COLUMN");
                        die();
                    } else {
                        Session::log($e->getMessage());
                        header("location:../index.php?errortype=SYSTEM_ERROR");
                        die();
                    }
                }
            }else {
                header("location:../index.php?errortype=REQUIRED_ALL");
                die();
            }
            break;
        }
        case "updateUser": {
            if (!Session::isLoggedIn()) {

            }
        }
    }
} else {
    http_response_code(404);
    include_once "../storage/error404.php";
    die();
}