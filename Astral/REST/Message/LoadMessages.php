<?php
require_once '../../app/config/config.php';
require_once '../../app/class/UTILS.php';
require_once '../../app/class/Session.php';
require_once '../../app/class/communication.php';

$user = $_REQUEST["pseudo"];
$user1 = $_REQUEST["pseudo1"];
$limit = $_REQUEST["limit"];
$skip = $_REQUEST["skip"];
try {
    $communication = Communication::getInstance($user, $user1);
    $b = Communication::getMessages($limit, $skip);

    if ($b != null) {

        for ($i = count($b) - 1; $i >= 0; $i--) {

            if ($user == $b[$i]->getPseudo()) {
                $usr = "You";
            } else {
                $usr = $b[$i]->getPseudo();
            }
            echo $usr . " said : " . $b[$i]->getMessage() . "<br>	";
        }
    } else {
        echo "0";
    }
} catch (PDOException $e) {
    Session::log($e->getMessage());
    if (_VERBOSE)
        throw $e;
}

?>