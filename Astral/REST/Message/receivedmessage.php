<?php
require_once '../../app/config/config.php';
require_once '../../app/class/UTILS.php';
require_once '../../app/class/Session.php';
require_once '../../app/class/communication.php';


$user = $_REQUEST["pseudo"];
$user1 = $_REQUEST["pseudo1"];
try {
    $communication = Communication::getInstance($user, $user1);
    $b = Communication::isMessageReceived(1000, 0, '0');
    Communication::seeNotification();
    if ($b == Null) {
        echo "0";
    } else {
        if (count($b) > 1) {
            echo "message";
        } else {
            echo $b[0]->getPseudo() . " said : " . $b[0]->getMessage();
        }
    }
} catch (PDOException $e) {
    Session::log($e->getMessage());
    if (_VERBOSE)
        throw $e;
}

?>