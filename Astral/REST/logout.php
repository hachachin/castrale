<?php
require_once "../app/class/Session.php";
require_once "../app/class/UTILS.php";
require_once "../app/config/config.php";

$token=filter_input(INPUT_POST,"token",FILTER_DEFAULT);

if(isset($token))
{
    global $s;
    $s = Session::getInstance();
    $s->start(_SESSION, _SECURE, _HTTPONLY);
    if($s->get("CSRF")==$token)
    {
        $s->destroySession();
        header("location:../index.php");
    }else
    {
        Session::log("Invalid User token, get back ","CSRF");
        echo"<h1>Invalid User token, get back <a href='../index.php'>Home</a> !</h1>";
    }
}else
{
    Session::log("Invalid Access to 'logout.php' without a token ","INVALID ACCESS");
    header("location:../index.php");
    die();
}
?>