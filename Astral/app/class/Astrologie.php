<?php
/**
* Astrologie
*/
class Astrologie
{
	private static $user1;
	private static $user2;
	private static $obj;
	static $Signe=[["capricorne","Verseau"],
                	["Verseau","Poissons"],
                	["Poissons","belier"],
                	["belier","Taureau"],
                	["Taureau","Gémaux"],
                	["Gémaux","Cancer"],
                	["Cancer","Lion"],
                	["Lion","Vierge"],
                	["Vierge","Balance"],
                	["Balance","Scorpion"],
                	["Scorpion","Sagittaire"],
                	["Sagittaire","capricorne"]];
							

	private function __construct($user1)
	{
		self::$user1=$user1;
	}

	public static function getInstance($user1,$user2=null)
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self($user1);

        }
        if ($user2!=null){
        	self::setPartner($user2);	
        }
        return self::$instance;
    }

    public static function setPartner($user2)
   	{	
    	self::$user2=$user2;
    }	

    public static function getSigne($dt){
        $dn=explode("-", $dt);
        $month=$dn[1];
        $day=explode(" ",$dn[2]);
        $day=$day[0];
        //echo $day;
        $compare=0;
        
        switch ($month) {
            case "01":
                $compare=20;
                break;
            case "02":
                $compare=20;
                break;
            case "03":
                $compare=21;
                break;
            case "04":
                $compare=21;
                break;
            case "05":
                $compare=22;
                break;
            case "06":
                $compare=22;
                break;
            case "07":
                $compare=24;
                break;
            case "08":
                $compare=24;
                break;
            case "09":
                $compare=21;
                break;
            case "10":
                $compare=23;
                break;
            case "11":
                $compare=23;
                break;
            case "12":
                $compare=22;
                break;
        }
        $signe="";
        if($day<=$compare){
            $signe=self::$Signe[$month-1][0];
        }else{
            $signe=self::$Signe[$month-1][1];
        }
        return $signe;
    }

}
?>