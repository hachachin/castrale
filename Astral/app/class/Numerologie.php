<?php
/**
* Numerologie
*/
require_once $_SERVER['DOCUMENT_ROOT']."/Astral/app/class/Entity/UTILISATEUR.php";


class Numerologie
{
	private static $user1;
	private static $user2;
	private static $obj;
	private static $instance;
	private static $compatibilite=[[33,66,100,33,100,66,66,33,100],
									[66,33,66,33,66,100,33,100,33],
									[100,66,33,33,66,100,33,66,100],
									[33,33,33,33,66,100,100,66,33],
									[100,66,66,66,66,33,33,66,66],
									[66,100,100,100,33,33,33,66,100],
									[66,33,33,100,33,33,66,66,100],
									[33,100,66,66,33,66,66,66,33],
									[100,33,100,33,66,100,100,33,33]];

							
	private function __construct($user1)
	{
		self::$user1=$user1;
	}

	public static function getInstance($user1,$user2=null)
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self($user1);

        }
        if ($user2!=null){
        	self::setPartner($user2);	
        }
        return self::$instance;
    }

    public static function setPartner($user2)
   	{	
    	self::$user2=$user2;
    }	

    public static function calcDN(){
    	$user1_dn=self::$user1->calculateDN();
    	$user2_dn=self::$user2->calculateDN();
    	$tot_dn=$user1_dn+$user2_dn;
    	return UTILISATEUR::sommeString(strval($tot_dn));
    }

    public static function calcFN(){
    	$user1_fn=self::$user1->calculateFullName();
    	$user2_fn=self::$user2->calculateFullName();
    	if ($user1_fn==0 || $user2_fn==0) {
    		return 50;
    	}elseif ($user2_fn==$user1_fn) {
    		return 100;
    	}else{
    		return 0;
    	}
    }

    public static function calcDiffOne($user){
    	$date=self::calcDN();
    	$user1_fn=$user->calculateFullName();
    	return 100-(abs($date-$user1_fn)*10);
    }

    public static function calcDiff(){
    	return (self::calcDiffOne(self::$user1)+self::calcDiffOne(self::$user2))/2;
    }

    public static function getCompatibiliteDate(){
    	return self::$compatibilite[self::$user1->calculateDN()-1][self::$user2->calculateDN()-1];
    }

    public static function getCompatibiliteNom(){
    	if(self::$user1->calculateFullName()==0||self::$user2->calculateFullName()==0)
    		return 50;

    	return self::$compatibilite[self::$user1->calculateFullName()-1][self::$user2->calculateFullName()-1];
    }

    public static function getNumerologieScore(){
    $total=self::calcFN()+self::calcDiff()+self::getCompatibiliteDate()+self::getCompatibiliteNom();
    return $total/4;
    }
}
?>