<?php
/**
* Class Message
*/
class MESSAGE
{
	private $pseudo;
	private $uti_pseudo;
	private $seen;
	private $date_message;
	private $message;
	private $vue;
	function __construct($message)
	{
	 $this->pseudo=$message[0];
	 $this->uti_pseudo=$message[1];
	 $this->seen=$message[2];
	 $this->date_message=$message[3];
	 $this->message=$message[4];
	 $this->vue=$message[5];
	}
	//------------GETTERS
	public function getPseudo(){ return $this->pseudo;}
	public function getUti_pseudo(){ return $this->uti_pseudo;}
	public function getSeen(){ return $this->seen;}
	public function getDate_message(){ return $this->date_message;}
	public function getMessage(){ return $this->message;}
	public function getVue(){ return $this->vue;}
	//--------------SETTERS
	public function setPseudo($var){ $this->pseudo=$var; }
	public function setUti_pseudo($var){ $this->uti_pseudo=$var; }
	public function setSeen($var){ $this->seen=$var; }
	public function setDate_message($var){ $this->date_message=$var; }
	public function setMessage($var){ $this->message=$var; }
	public function setVue($var){ $this->vue=$var; }
}
?>