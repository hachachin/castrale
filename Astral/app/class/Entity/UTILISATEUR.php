<?php

/**
 * Created by PhpStorm.
 * User: chadi
 * Date: 26/04/18
 * Time: 20:17
 */

require_once $_SERVER['DOCUMENT_ROOT']."/Astral/app/class/Astrologie.php";

class UTILISATEUR
{
    private $PSEUDO;
    private $GENDER;
    private $NOM;
    private $PRENOM;
    private $MDP;
    private $EMAIL;
    private $DATE_NAISSANCE;
    private $LIEU_NISSANCE;
    private $DESCRIPTION;
    private $LOCALIZATION;
    private $DERNIER_CONNEXION;
    private $NBRE_VUE;

    /**
     * UTILISATEUR constructor.
     * @param $PSEUDO
     * @param $GENDER
     * @param $NOM
     * @param $PRENOM
     * @param $MDP
     * @param $EMAIL
     * @param $DATE_NAISSANCE
     * @param $LIEU_NISSANCE
     * @param $DESCRIPTION
     * @param $LOCALIZATION
     * @param $DERNIER_CONNEXION
     * @param $NBRE_VUE
     */
    public function __construct($tab)
    {

        $this->PSEUDO = $tab[0];
        $this->GENDER = $tab[1];
        $this->NOM = $tab[2];
        $this->PRENOM = $tab[3];
        $this->MDP = $tab[4];
        $this->EMAIL = $tab[5];
        $this->DATE_NAISSANCE = $tab[6];
        $this->LIEU_NISSANCE = $tab[7];
        $this->DESCRIPTION = $tab[8];
        $this->LOCALIZATION = $tab[9];
        $this->DERNIER_CONNEXION = $tab[10];
        $this->NBRE_VUE = $tab[11];
    }


    /**
     * @return mixed
     */
    public function getPSEUDO()
    {
        return $this->PSEUDO;
    }

    /**
     * @param mixed $PSEUDO
     */
    public function setPSEUDO($PSEUDO)
    {
        $this->PSEUDO = $PSEUDO;
    }

    /**
     * @return mixed
     */
    public function getGENDER()
    {
        return $this->GENDER;
    }

    /**
     * @param mixed $GENDER
     */
    public function setGENDER($GENDER)
    {
        $this->GENDER = $GENDER;
    }

    /**
     * @return mixed
     */
    public function getNOM()
    {
        return $this->NOM;
    }

    /**
     * @param mixed $NOM
     */
    public function setNOM($NOM)
    {
        $this->NOM = $NOM;
    }

    /**
     * @return mixed
     */
    public function getPRENOM()
    {
        return $this->PRENOM;
    }

    /**
     * @param mixed $PRENOM
     */
    public function setPRENOM($PRENOM)
    {
        $this->PRENOM = $PRENOM;
    }

    /**
     * @return mixed
     */
    public function getMDP()
    {
        return $this->MDP;
    }

    /**
     * @param mixed $MDP
     */
    public function setMDP($MDP)
    {
        $this->MDP = $MDP;
    }

    /**
     * @return mixed
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }

    /**
     * @param mixed $EMAIL
     */
    public function setEMAIL($EMAIL)
    {
        $this->EMAIL = $EMAIL;
    }

    /**
     * @return mixed
     */
    public function getDATENAISSANCE()
    {
        return $this->DATE_NAISSANCE;
    }

    /**
     * @param mixed $DATE_NAISSANCE
     */
    public function setDATENAISSANCE($DATE_NAISSANCE)
    {
        $this->DATE_NAISSANCE = $DATE_NAISSANCE;
    }

    /**
     * @return mixed
     */
    public function getLIEUNISSANCE()
    {
        return $this->LIEU_NISSANCE;
    }

    /**
     * @param mixed $LIEU_NISSANCE
     */
    public function setLIEUNISSANCE($LIEU_NISSANCE)
    {
        $this->LIEU_NISSANCE = $LIEU_NISSANCE;
    }

    /**
     * @return mixed
     */
    public function getDESCRIPTION()
    {
        return $this->DESCRIPTION;
    }

    /**
     * @param mixed $DESCRIPTION
     */
    public function setDESCRIPTION($DESCRIPTION)
    {
        $this->DESCRIPTION = $DESCRIPTION;
    }

    /**
     * @return mixed
     */
    public function getLOCALIZATION()
    {
        return $this->LOCALIZATION;
    }

    /**
     * @param mixed $LOCALIZATION
     */
    public function setLOCALIZATION($LOCALIZATION)
    {
        $this->LOCALIZATION = $LOCALIZATION;
    }

    /**
     * @return mixed
     */
    public function getDERNIERCONNEXION()
    {
        return $this->DERNIER_CONNEXION;
    }

    /**
     * @param mixed $DERNIER_CONNEXION
     */
    public function setDERNIERCONNEXION($DERNIER_CONNEXION)
    {
        $this->DERNIER_CONNEXION = $DERNIER_CONNEXION;
    }

    /**
     * @return mixed
     */
    public function getNBREVUE()
    {
        return $this->NBRE_VUE;
    }

    /**
     * @param mixed $NBRE_VUE
     */
    public function setNBREVUE($NBRE_VUE)
    {
        $this->NBRE_VUE = $NBRE_VUE;
    }


    public static function sommeString($st){

        $st_exploded=str_split($st);
        $value_string=10;
        while($value_string>9)
        {
            $value_string=0;
            foreach ($st_exploded as $key => $value)
            {
                $value_string+=intval($value);
            }
            $st_exploded=str_split(strval($value_string));
        }
        return $value_string;
    }

    public function calculateDN(){
        return $this->sommeString($this->DATE_NAISSANCE);
    }

    public function calculateFullName(){
        $name=$this->NOM.$this->PRENOM;
        $name=str_split($name);

        $value_name=0;
        if (strcasecmp($this->GENDER,"M")==0){
            foreach ($name as $key => $value) {

                if (strcasecmp ($value,'f')==0 || strcasecmp ($value,'o')==0 || strcasecmp ($value,'x')==0) {
                    $value_name++;
                }

            }
        }elseif (strcasecmp($this->GENDER,"F")==0) {
            foreach ($name as $key => $value) {

                if (strcasecmp ($value,'e')==0 || strcasecmp ($value,'n')==0 || strcasecmp ($value,'w')==0) {
                    $value_name++;
                }

            }
        }

        return $this->sommeString(strval($value_name));
    }

    public static function getNumberAlphabet($str){
        $ord=ord($str);
        if(ord($str)>91) {
            $ord=ord($str)-32;
        }
        $value=($ord-65);
        return ($value%9)+1;
    }

    public function calculateName(){
        $name=$this->PRENOM;
        $name=str_split($name);
        $value_name=0;
        foreach ($name as $key => $value) {

            $value_name+=$this->getNumberAlphabet($value);
        }

        return $this->sommeString(strval($value_name));
    }

    public function getSigne(){
        return Astrologie::getSigne($this->DATE_NAISSANCE);
    }



}