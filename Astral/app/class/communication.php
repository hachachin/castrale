<?php
/**
* 
*/
require_once "Entity/MESSAGE.php";

class Communication
{
	private static $pseudo1;
	private static $pseudo2;
	private static $obj;
	private static $instance;
	private function __construct($pseudo1)
	{
		self::$pseudo1=$pseudo1;
		self::$obj=new UTILS();
	}
	public static function getInstance($pseudo,$pseudo2)
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self($pseudo);

        }
     	self::setReceiver($pseudo2);
        return self::$instance;
    }
    public static function setReceiver($pseudo1)
    {
    	self::$pseudo2=$pseudo1;
    }
	public static function sendMessage($Message){
		$message=array();
		$message[]=self::$pseudo1;
		$message[]=self::$pseudo2;
		$message[]="0";
		$message[]=date("Y-m-d H:i:s");
		$message[]=$Message;
		self::$obj->add("MESSAGE",$message);
		return true;
	}
	public static function deleteMessage($Message,$date){
		$message3=array();
	    $message3[]="MESSAGE";
	    $message3[]="DATE_MESSAGE";
		$message=array();
		$message[]=$Message;
	    $message[]=$date;
	    $message4=array();
	    $message4[]="=";
	    $message4[]="=";
	    $message5=array();
	    $message5[]="and";
		self::$obj->delete("MESSAGE",$message3,$message,$message4,$message5);
	}
	public static function seeMessage($Message,$date){
		$set=array();
	    $set[]="SEEN";
	    $set1=array();
	    $set1[]="1";
	    $set2=array();
	    $set2[]="MESSAGE";
	    $set2[]="DATE_MESSAGE";
	    $set2[]="PSEUDO";
	    $set2[]="UTI_PSEUDO";
	    $set3=array();
	    $set3[]=$Message;
	    $set3[]=$date;
	    $set3[]=self::$pseudo1;
	    $set3[]=self::$pseudo2;
	    $set4=array();
	    $set4[]="=";
	    $set4[]="=";
	    $set4[]="=";
	    $set4[]="=";
	    $set5=array();
	    $set5[]="and";
	    $set5[]="and";
	    $set5[]="and";
    	self::$obj->update("MESSAGE",$set,$set1,$set2,$set3,$set4,$set5);
	}
	public static function getMessages($limit,$skip){
	
	    $champs = array();
	    $champs[] = "PSEUDO";
	    $champs[] = "UTI_PSEUDO";
	    $champs[] = "PSEUDO";
	    $champs[] = "UTI_PSEUDO";
	    $test = array();
	    $test[] = "=";
	    $test[] = "=";
	    $test[] = "=";
	    $test[] = "=";
	    $t = array();

	    $t[] = self::$pseudo1;
	    $t[] = self::$pseudo2;
	    $t[] = self::$pseudo2;
	    $t[] = self::$pseudo1;
	    $log = array();
	    $log[] = "and";
	    $log[] = "and";
	    $log[] = "or";
	    $log[] = "or";
	    $addition="order by DATE_MESSAGE desc  limit $skip,$limit";
	    return self::$obj->selectCondition("MESSAGE", $champs, $t, $test,$addition, $log,true);
	}
	
	public static function getMessagesType($limit,$skip,$type){
	
	    $champs = array();
	    $champs[] = "PSEUDO";
	    $champs[] = "UTI_PSEUDO";
	    $champs[] = "PSEUDO";
	    $champs[] = "UTI_PSEUDO";
	    $champs[] = "SEEN";
	    $test = array();
	    $test[] = "=";
	    $test[] = "=";
	    $test[] = "=";
	    $test[] = "=";
	    $test[] = "=";
	    $t = array();

	    $t[] = self::$pseudo1;
	    $t[] = self::$pseudo2;
	    $t[] = self::$pseudo2;
	    $t[] = self::$pseudo1;
	    $t[] = $type;
	    $log = array();
	    $log[] = "and";
	    $log[] = "or";
	    $log[] = "or";
	    $log[] = "or";
	    $log[] = "and";
	    $addition="order by DATE_MESSAGE desc  limit $skip,$limit";
	    return self::$obj->selectCondition("MESSAGE", $champs, $t, $test,$addition, $log,true);
	}
	public static function openConversation(){
		$set=array();
	    $set[]="seen_message";
	    $set1=array();
	    $set1[]="1";
	    $set2=array();
	    $set2[]="UTI_PSEUDO";
	    $set2[]="PSEUDO";
	    $set3=array();
	    $set3[]=self::$pseudo1;
	    $set3[]=self::$pseudo2;
	    $set4=array();
	    $set4[]="=";
	    $set4[]="=";
	    $set5=array();
	    $set5[]="and";
    	self::$obj->update("MESSAGE",$set,$set1,$set2,$set3,$set4,$set5);
	}
	public static function seeNotification(){
		$set=array();
	    $set[]="SEEN";
	    $set1=array();
	    $set1[]="1";
	    $set2=array();
	    $set2[]="UTI_PSEUDO";
	    $set2[]="PSEUDO";
	    $set3=array();
	    $set3[]=self::$pseudo1;
	    $set3[]=self::$pseudo2;
	    $set4=array();
	    $set4[]="=";
	    $set4[]="=";
	    $set5=array();
	    $set5[]="and";
    	self::$obj->update("MESSAGE",$set,$set1,$set2,$set3,$set4,$set5);
	}
	public static function isMessageReceived($limit,$skip,$type){
	
	    $champs = array();
	    $champs[] = "UTI_PSEUDO";
	    $champs[] = "SEEN";
	    $test = array();
	    $test[] = "=";
	    $test[] = "=";
	    $t = array();
	    $t[] = self::$pseudo1;
	    $t[] = $type;
	    $log = array();
	    $log[] = "and";
	    $log[] = "and";
	    $addition="order by DATE_MESSAGE desc  limit $skip,$limit";
	    return self::$obj->selectCondition("MESSAGE", $champs, $t, $test,$addition, $log,true);
	}
}
?>