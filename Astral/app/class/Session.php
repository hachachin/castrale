<?php

/**
 * Created by PhpStorm.
 * User: chadi
 * Date: 28/04/18
 * Time: 11:32
 */
class Session
{
    const STARTED = TRUE;
    const NOT_STARTED = FALSE;

    private $sessionState = self::NOT_STARTED;
    private static $instance;


    public function __construct()
    {

    }

    public static function getInstance()
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function start($name, $secure = true,$httponly = true) {

        if (ini_set('session.use_only_cookies', 1) === FALSE)
            return false;

        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"],
            $cookieParams["domain"], $secure, $httponly);
        session_name($name);

        if ($this->sessionState !== self::STARTED)
            $this->sessionState = session_start();

        //session_regenerate_id(true);
        return ($this->sessionState === self::STARTED);
    }

    public function closeSession() {
        if ($this->sessionState !== self::NOT_STARTED) {
            session_write_close();
            $this->sessionState = self::NOT_STARTED;
        }

        return ($this->sessionState === self::NOT_STARTED);
    }

    public function destroySession()
    {
        if ($this->sessionState !== self::NOT_STARTED) {
            unset($_SESSION);
            $params = session_get_cookie_params();
            setcookie(session_name(),'', time() - 42000,$params["path"],
                $params["domain"],$params["secure"],$params["httponly"]);
            $this->sessionState = !session_destroy();

            return ($this->sessionState === self::NOT_STARTED);
        }
        return FALSE;
    }

    public function set($name , $value) {
        $_SESSION[$name] = $value;
    }

    public function get($name) {
        if (isset($_SESSION[$name]))
            return $_SESSION[$name];
        return null;
    }

    public function is_set($name) {
        return isset($_SESSION[$name]);
    }

    public function un_set($name) {
        unset($_SESSION[$name]);
    }

    public function checkCSRF($token) {
        if (isset($token) || ($this->sessionState !== self::STARTED))
            return false;
        return ($this->get("CSRF") == $token);
    }

    public static function newCSRF() {
        $token = md5(uniqid(rand(), true));

        if (!isset($token))
            throw new Exception('CREATING USER CSRF TOKEN FAILED');

        return $token;
    }

    public static function logOutRedirect($sessionName, $target) {
        $Session = self::getInstance();
        $Session->start($sessionName);
        $Session->destroySession();
        header('Location: ' . $target);
        die();
    }

    public static function  isActive($name)
    {
        global $S;
        $S=Session::getInstance();
        $S->start($name,_SECURE,_HTTPONLY);

        $now = date_create(date("Y-m-d H:i:s"));
        $timeDiffrence=date_diff(date_create($S->get("LAST_ACTIVITY")),$now);

        if($timeDiffrence->i > 10)
        {
            return false;
        }

        $S->set("LAST_ACTIVITY",date("Y-m-d H:i:s"));
        return true;
    }

    public static function isLoggedIn($target=null)
    {
        global $S;
        $S=Session::getInstance();
        $S->start(_SESSION,_SECURE,_HTTPONLY);

        if($S->is_set("CSRF") && $S->is_set("USER") && self::isActive(_SESSION) )
        {
            if($target!=null)
            header("location:".$target);
            return true;
        }
        return false;
    }

    public static function log($msg,$attack=false)
    {
        try {
            $f = fopen($_SERVER["DOCUMENT_ROOT"]."/Astral/storage/Errorlog.txt", "a+")
                    or new Exception("CANT OPEN ERROR LOG");

            $msg="[".date("Y-m-d H:i:s") . "]-".$msg;

            if($attack!=false)
                $msg.=" | ![$attack]! ip: " .$_SERVER["REMOTE_ADDR"];

            $msg.=" on:'".$_SERVER["SCRIPT_FILENAME"]."' | Signature: ".$_SERVER['HTTP_USER_AGENT']."\n";

            fwrite($f, $msg);
            fclose($f);
        }catch(Exception $e)
        {
            throw $e;
        }
    }


}