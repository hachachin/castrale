<?php
/**
* 
*/
//require_once "../config/config.php";

class chercher
{
	private static $user1;
	private static $user2;
	private static $obj;
	private static $instance;

	private function __construct($user1)
	{
		self::$user1=$user1;
	}

	public static function getInstance($user1,$user2=null)
    {
        if ( !isset(self::$instance))
        {
            self::$instance = new self($user1);
            self::$obj=new UTILS();

        }
        if ($user2!=null){
        	self::setPartner($user2);	
        }
        return self::$instance;
    }

    public static function setPartner($user2)
   	{	
    	self::$user2=$user2;
    }	

    public static function getDistanceFromLatLonInKm($lat1,$lon1,$lat2,$lon2) {
  	  $R = 6371; 
  	  $dLat = deg2rad($lat2-$lat1);
	  $dLon = deg2rad($lon2-$lon1);
	  $a = 
	    sin($dLat/2) * sin($dLat/2) +
	    cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * 
	    sin($dLon/2) * sin($dLon/2)
	    ; 
	  $c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
	  $d = $R * $c; 
	  
	  return $d;
	}

	public static function deg2rad($deg) {
  			return $deg * (pi()/180);
	}

	public static function getDistance()
	{
		$coordone1=self::$user1->getLOCALIZATION();
		$coordone2=self::$user2->getLOCALIZATION();
		$coordone1=explode(",",$coordone1);
		$coordone2=explode(",",$coordone2);
		return self::getDistanceFromLatLonInKm($coordone1[0],$coordone1[1],$coordone2[0],$coordone2[1]);
	}

	public static function getClosePerson(){
		$champs = array();
	    $champs[] = "PSEUDO";
	    $champs[] = "GENDER";
	    $champs[] = "ORIENTATION";
	    $test = array();
	    $test[] = "<>";
	    $test[] = "=";
	    $test[] = "=";
	    $t = array();
	    $t[] = self::$user1->getPseudo();
	    $t[] = self::$user1->getORIENTATION();
	    $t[] = self::$user1->getGENDER();
	    $log = array();
	    $log[] = "and";
	    $log[] = "and";
	    $log[] = "and";
	    $addition="";
	    $Person=self::$obj->selectCondition("UTILISATEUR", $champs, $t, $test,$addition, $log);
	    $closePerson = array();
	    foreach ($Person as $key => $value) {
	    	self::setPartner($value);
	    	echo self::getDistance()."<br>";
	    	if(self::getDistance()<_DISTANCE){
	    		array_push($closePerson, $value);
	    	}
	    }
	    return $closePerson;
	}

	public static function scoreResult($array_to_score){
		function compare_score($a, $b) {
    		$score1 = $a['score'];
    		$score2 = $b['score'];
    		return ($score1 < $score2) ? -1 : 1;
			}
		usort($array_to_score, "compare_score");
		return $array_to_score;
	}

	public static function getNumerologie(){
		$closePerson=self::getClosePerson();
		$SuggestNumerologie=array();
		foreach ($closePerson as $key => $value) {
			Numerologie::getInstance(self::$user1,$value);
			array_push($SuggestNumerologie, ["user"=>$value,"score"=>Numerologie::getNumerologieScore()]);
			
		}
		$SuggestNumerologie=self::scoreResult($SuggestNumerologie);
		return $SuggestNumerologie;
	}
}
?>