<?php

class One_instance {

  /**
   * @var One_instance
   * @access private
   * @static
   */
   private static $_instance = null;
   public static $_cnx = null;

   /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
   private function __construct($host,$dbname,$user,$password) {
    try {
      self::$_cnx=new PDO("mysql:host=".$host.";dbname=".$dbname."",$user,$password);
      self::$_cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (PDOException $e) {
        if(_VERBOSE)
            Session::log($e->getMessage() . "-code: " . $e->getCode());

        throw $e;
    }

   }

   /**
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return One_instance
    */
   public static function getInstance()
   {
       global $_datasrc;

       if(is_null(self::$_instance))
       {
          self::$_instance = new One_instance($_datasrc['host'],$_datasrc['db_name'],$_datasrc['username'],$_datasrc['password']);
       }

        return self::$_cnx;
    }

}