<?php
/**
 * Created by PhpStorm.
 * User: chadi
 * Date: 26/04/18
 * Time: 20:28
 */

require_once "Entity/UTILISATEUR.php";
require_once "Singleton.php";


class UTILS
{
    private $con;

    public function __construct()
    {
        $this->con = One_instance::getInstance();
    }

    public function add($classe, $t)
    {
        try {
            $i = $this->nbCol($classe, "");

            $values = null;
            $val = null;

            for ($j = 0; $j < count($t); $j++) {
                if ($j == 0) {
                    $values = $i[$j];
                    $val = "'" . $t[$j] . "'";
                } else {
                    $values = $values . "," . $i[$j];
                    $val = $val . "," . "'" . $t[$j] . "'";
                }
            }
            $req = "insert into " . $classe . " (" . $values . ") values (" . $val . ")";
            //echo $req;
            $this->con->exec($req);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function nbCol($c, $id)
    {
        try {
            $requete = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $c . "' and COLUMN_NAME <>'" . $id . "'";
            $b = array();

            if ($res = $this->con->query($requete)) {
                while ($ligne = $res->fetch(PDO::FETCH_ASSOC)) {
                    foreach ($ligne as $key) {
                        $b[] = $key;
                    }
                }
            }
            return $b;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getList($c)
    {
        try {
            $requete = "select * from " . $c;//reqette select
            $b = array();

            if ($res = $this->con->query($requete)) {
                while ($ligne = $res->fetch()) {

                    $b[] = new $c($ligne);
                }
            }
            return $b;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function update($classe, $champsupdate, $valueupdate, $champs, $test, $t, $l)
    {
        try {
            $upd = "";
            $te = "";

            for ($i = 0; $i < count($champsupdate); $i++) {
                if ($i == count($champsupdate) - 1) {
                    $upd = $upd . " " . $champsupdate[$i] . "='" . $valueupdate[$i] . "'";
                } else {
                    $upd = $upd . " " . $champsupdate[$i] . "='" . $valueupdate[$i] . "',";
                }
            }

            for ($i = 0; $i < count($champs); $i++) {
                if ($i == count($champs) - 1) {
                    $te = $te . " " . $champs[$i] . " " . $t[$i] . " '" . $test[$i] . "'";
                } else {
                    $te = $te . " " . $champs[$i] . " " . $t[$i] . " '" . $test[$i] . "' " . $l[$i];
                }
            }

            $requete = "update " . $classe . " set " . $upd . " where " . $te;

            $this->con->exec($requete);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function select($classe, $champs, $test, $t)
    {
        try {
            $reqq = "select * from " . $classe . " where ";
            for ($i = 0; $i < count($champs); $i++) {
                if ($i != count($champs) - 1) {
                    $reqq = $reqq . $champs[$i] . $t[$i] . "'" . $test[$i] . "' and ";
                } else {
                    $reqq = $reqq . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                }
            }
            $b = null;
            if ($res = $this->con->query($reqq)) {
                $b = array();
                while ($ligne = $res->fetch()) {
                    $b[] = new $classe($ligne);
                }
            }
            return $b;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function sort($champs, $test, $t, $log)
    {
        for ($i = count($champs) - 1; $i > 0; $i--) {
            for ($k = 0; $k <= $i - 1; $k++) {
                if ($champs[$k + 1] < $champs[$k]) {
                    $c = $champs[$k];
                    $champs[$k] = $champs[$k + 1];
                    $champs[$k + 1] = $c;
                    $c = $test[$k];
                    $test[$k] = $test[$k + 1];
                    $test[$k + 1] = $c;
                    $c = $t[$k];
                    $t[$k] = $t[$k + 1];
                    $t[$k + 1] = $c;
                    $c = $log[$k];
                    $log[$k] = $log[$k + 1];
                    $log[$k + 1] = $c;
                }
            }
        }
    }

    public function selectCondition($classe, $champs, $test, $t, $addition, $log, $orname = false)
    {
        try {
            $reqq = "select * from " . $classe;
            if ($orname) {
                for ($i = count($champs) - 1; $i > 0; $i--) {
                    for ($k = 0; $k <= $i - 1; $k++) {
                        if ($champs[$k + 1] < $champs[$k]) {
                            $c = $champs[$k];
                            $champs[$k] = $champs[$k + 1];
                            $champs[$k + 1] = $c;
                            $c = $test[$k];
                            $test[$k] = $test[$k + 1];
                            $test[$k + 1] = $c;
                            $c = $t[$k];
                            $t[$k] = $t[$k + 1];
                            $t[$k + 1] = $c;
                            $c = $log[$k];
                            $log[$k] = $log[$k + 1];
                            $log[$k + 1] = $c;
                        }
                    }
                }

            }
            if (count($champs) > 0) {
                for ($i = 0; $i < count($champs); $i++) {

                    if ($i == 0) {
                        if (strstr($test[$i], 'select')) {
                            $reqq = $reqq . ' where (' . $champs[$i] . $t[$i] . "" . $test[$i] . "";
                        } else {
                            $reqq = $reqq . ' where (' . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                        }

                    } else if ($i != count($champs) - 1) {
                        if ($log[$i] == 'and') {
                            if (strstr($test[$i], 'select')) {
                                $reqq = $reqq . ' ) ' . $log[$i] . ' (' . $champs[$i] . $t[$i] . "" . $test[$i] . "";
                            } else {
                                $reqq = $reqq . ' ) ' . $log[$i] . ' (' . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                            }
                        } else {
                            if (strstr($test[$i], 'select')) {
                                $reqq = $reqq . ' ' . $log[$i] . ' ' . $champs[$i] . $t[$i] . "" . $test[$i] . "";
                            } else {
                                $reqq = $reqq . ' ' . $log[$i] . ' ' . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                            }
                        }
                    } else
                        if ($log[$i] == "and") {
                            if (strstr($test[$i], 'select')) {
                                $reqq = $reqq . ' ) ' . $log[$i] . ' (' . $champs[$i] . $t[$i] . "" . $test[$i] . "";
                            } else {
                                $reqq = $reqq . ' ) ' . $log[$i] . ' (' . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                            }
                        } else {
                            if (strstr($test[$i], 'select')) {
                                $reqq = $reqq . ' ' . $log[$i] . ' ' . $champs[$i] . $t[$i] . "" . $test[$i] . "";
                            } else {
                                $reqq = $reqq . ' ' . $log[$i] . ' ' . $champs[$i] . $t[$i] . "'" . $test[$i] . "'";
                            }
                        }
                }
            }
            if (count($champs) > 0) {
                $reqq = $reqq . ') ' . $addition;
            } else {
                $reqq = $reqq . ' ' . $addition;
            }

            $b = null;
            if ($res = $this->con->query($reqq)) {
                $b = array();
                while ($ligne = $res->fetch()) {

                    $b[] = new $classe($ligne);

                }
            }
            return $b;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function delete($classe, $champs, $test, $t, $l)
    {
        try {
            $te = "";
            for ($i = 0; $i < count($champs); $i++) {
                if ($i == count($champs) - 1) {
                    $te = $te . " " . $champs[$i] . " " . $t[$i] . " '" . $test[$i] . "'";
                } else {
                    $te = $te . " " . $champs[$i] . " " . $t[$i] . " '" . $test[$i] . "' " . $l[$i];
                }
            }

            $requete = "delete from " . $classe . " where" . $te;
            $this->con->exec($requete);
        } catch (PDOException $e) {
            throw $e;
        }
    }


}

?>
