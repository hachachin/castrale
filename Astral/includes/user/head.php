	<title>
		<?=$_website['title'];?>
	</title>
	<meta name="description" content="<?=$_website['description'];?>">
	<meta name="author" content="<?=$_website['author']?>">
	<meta name="designer" content="<?=$_website['designer'];?>">
	<meta name="copyright" content="<?=$_website['copyright']?>">
	<meta name="language" content="en-US">
	
	<link href="<?=$_paths['assets']?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=$_paths['assets']?>css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=$_paths['assets']?>css/jquery-confirm.min.css" rel="stylesheet">
