
	<footer>
		<div class="container">
			<div class="row m-top-md m-bottom-lg">
				<div class="col-sm m-top-sm">
					<address class="m-bottom-sm">
						<span class="fa fa-fw fa-map-marker"></span> <?=$_website['address']?>
					</address>
					<div>
						<a href="mailto:<?=$_website['email']?>" class="text-light" target="_blank"><span class="fa fa-fw fa-envelope"></span> <?=$_website['email']?></a>
					</div>
					<div>
						<a href="tel:<?=$_website['phone']['ISO']?>" class="text-light" target="_blank"><span class="fa fa-fw fa-phone"></span> <?=$_website['phone']['text']?></a>
					</div>
				</div>
				<div class="col-sm m-top-sm text-right">
					<a href="#" target="_blank" class="text-light">F.A.Q <span class="fa fa-fw fa-question-circle-o"></span></a><br/>
					<a href="#" target="_blank" class="text-light">Privacy Policy <span class="fa fa-fw fa-list-ul"></a><br/>
					<a href="#" target="_blank" class="text-light">Terms Of Service <span class="fa fa-fw fa-list-alt"></a>
				</div>
			</div>
			<div class="row m-bottom-lg">
				<div class="col">
					<div class="small text-center">
						<?=$_website['copyright']?>
					</div>
					<div class="small text-center text-secondary">
						Made with <span class="fa fa-fw fa-heart"></span> by
						<?php foreach ($_authors as $k => $author): ?>
						<button class="site-author-btn" onclick="showEmail(<?=$k?>)"><?=$author['name']?></button> |
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<script src="<?=$_paths['assets']?>js/jquery-3.2.1.min.js"></script>
	<script src="<?=$_paths['assets']?>js/bootstrap.min.js"></script>
