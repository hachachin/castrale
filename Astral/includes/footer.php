<footer class="footer">
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <nav>
                    <ul>
                        <li>
                            <a href="index.php">
                                <span class="fa fa-star-half"></span> <?=$_website['brand_alpha']?>
                            </a>
                        </li>
                        <li>
                            <a href="about.php">
                                About Us
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom"
                               href="#" target="_blank">
                                <span class="fa fa-facebook-square"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="copyright col-lg-3">
                <?=$_website['copyright']?>
            </div>
        </div>
    </div>
</footer>

<script src="<?=$_paths['assets']?>js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?=$_paths['assets']?>js/popper.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=$_paths['assets']?>js/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?=$_paths['assets']?>js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?=$_paths['assets']?>js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="<?=$_paths['assets']?>js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="<?=$_paths['assets']?>js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<!-- bootstrap datepicker -->
<script src="<?=$_paths['assets']?>js/bootstrap-datepicker.js"></script>

<script src="<?=$_paths['assets']?>js/UTILS.js" type="text/javascript"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAUgumISPtSPc0fpPewP76vsmRIRNRsRs&callback=initMap">
</script>
<!-- BS JavaScript -->
<script src="<?=$_paths['assets']?>js/countries.js"></script>
<script>
    populateCountries("country", "state");
</script>

