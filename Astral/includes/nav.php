<nav id="mainNav" class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="50">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" rel="tooltip" title="Coeur Astral !!! Nouveau façon de recontre"
               data-placement="bottom" target="_blank">
                <span class="fa fa-star-half"></span> <?= $_website['brand'] ?>
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation"
             data-nav-image="assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <button id="log-btn" type="button" class="btn btn-round btn-block" data-toggle="modal"
                            data-target="#Modal-log">
                        SE CONNECTER <i class="fa fa-sign-in "></i>
                    </button>
                </li>

            </ul>
        </div>
    </div>
</nav>