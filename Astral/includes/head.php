<meta charset="utf-8"/>
<link rel="apple-touch-icon" sizes="76x76" href="<?= $_paths['assets'] ?>img/favicon.ico">
<link rel="icon" type="image/png" href="<?= $_paths['assets'] ?>img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>
    <?=$_website['title'];?>
</title>
<meta name="description" content="<?=$_website['description'];?>">
<meta name="author" content="<?=$_website['author']?>">
<meta name="designer" content="<?=$_website['designer'];?>">
<meta name="copyright" content="<?=$_website['copyright']?>">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
      name='viewport'/>
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
<!-- CSS Files -->
<link href="<?= $_paths['assets'] ?>css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?= $_paths['assets'] ?>css/now-ui-kit.css?v=1.1.0" rel="stylesheet"/>
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= $_paths['assets'] ?>css/datepicker3.css">
<link rel="stylesheet" href="<?= $_paths['assets'] ?>css/countrySelect.css">